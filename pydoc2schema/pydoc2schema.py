"""Main module."""
import glob, os
import importlib.util
import logging
from copy import deepcopy
import re

import docstring_parser

def load_directory(directory, file_ending="py"):
    """
    Enumerate directory finding all the python files

    Arguments:
        - directory (string): is a directory name

    Returns:
        Array of file names
    """
    
    if os.path.isfile(directory): return [directory]
    return glob.glob(directory + "/**/*." + file_ending, recursive=True)

def parse_extract_format(parameters):
    if not parameters: return parameters
    if not "x-type" in parameters or parameters["x-type"] == None:
        parameters["x-type"] = "str"
    parameters["x-type"] = parameters["x-type"].lower()
    # string
    if parameters["x-type"] == "str" or parameters["x-type"] == "string":
        parameters["type"] = "string"
    # int
    elif parameters["x-type"] == "int" or parameters["x-type"] == "integer":
        parameters["type"] = "integer"
    # boolean
    elif parameters["x-type"] == "bool" or parameters["x-type"] == "boolean":
        parameters["type"] = "boolean"
    # number
    elif parameters["x-type"] == "number" or parameters["x-type"] == "numeric": 
        parameters["type"] = "number"
    # double
    elif parameters["x-type"] == "double":
        parameters["type"] = "number"
        parameters["format"] = "double"
    # int32
    elif parameters["x-type"] == "int32":
        parameters["type"] = "number"
        parameters["format"] = "int32"
    # int64
    elif parameters["x-type"] == "int64":
        parameters["type"] = "number"
        parameters["format"] = "int64"
    # date-time
    elif parameters["x-type"] == "datetime" or parameters["x-type"] == "date-time":
        parameters["type"] = "string"
        parameters["format"] = "date-time"
    # date
    elif parameters["x-type"] == "date":
        parameters["type"] = "string"
        parameters["format"] = "date"
    # time
    elif parameters["x-type"] == "time":
        parameters["type"] = "string"
        parameters["format"] = "time"
    # email
    elif parameters["x-type"] == "email":
        parameters["type"] = "string"
        parameters["format"] = "email"
    # hostname
    elif parameters["x-type"] == "hostname":
        parameters["type"] = "string"
        parameters["format"] = "hostname"
    # idn-hostname
    elif parameters["x-type"] == "idn-hostname":
        parameters["type"] = "string"
        parameters["format"] = "idn-hostname"        
    # ipv4
    elif parameters["x-type"] == "ipv4":
        parameters["type"] = "string"
        parameters["format"] = "ipv4"
    # ipv6
    elif parameters["x-type"] == "ipv6":
        parameters["type"] = "string"
        parameters["format"] = "ipv6"
    # uri
    elif parameters["x-type"] == "uri":
        parameters["type"] = "string"
        parameters["format"] = "uri"
    # uri-reference
    elif parameters["x-type"] == "uri-reference":
        parameters["type"] = "string"
        parameters["format"] = "uri-reference"        
    # iri
    elif parameters["x-type"] == "iri":
        parameters["type"] = "string"
        parameters["format"] = "iri"
    # iri-reference
    elif parameters["x-type"] == "iri-reference":
        parameters["type"] = "string"
        parameters["format"] = "iri-reference"        
    # uri-template
    elif parameters["x-type"] == "uri-template":
        parameters["type"] = "string"
        parameters["format"] = "uri-template"
    # iri
    elif parameters["x-type"] == "iri":
        parameters["type"] = "string"
        parameters["format"] = "iri"
    # iri
    elif parameters["x-type"] == "relative-json-pointer":
        parameters["type"] = "string"
        parameters["format"] = "relative-json-pointer"                
    # json-pointer
    elif parameters["x-type"] == "json-pointer":
        parameters["type"] = "string"
        parameters["format"] = "json-pointer"
    # object
    elif parameters["x-type"] == "object" or parameters["x-type"] == "obj" or parameters["x-type"] == "dict" or parameters["x-type"] == "dictionary":
        parameters["type"] = "object"
    else:
        parameters["type"] = "object" # the input type may be a schema type
    return parameters

def lookup_schema(schema_name, schemas):
    schema_file = None
    if schema_name != None: # has the docstring a return statement
        for schema_file_name in schemas:
            if schema_file_name.find(schema_name.replace(".json", "")) >= 0:
                schema_file = schema_file_name
        if not schema_file:
            print ("Warning: could not find schema file " + schema_name + ". Found these " + str(schemas.keys()))
    return schema_file

def parse_docstring(doc, schemas={}):
    docstring = ""
    parseddoc = {}
    try: docstring = docstring_parser.parse(doc, style=docstring_parser.Style.google)
    except: pass    
    if docstring:
        parseddoc["description"] = docstring.long_description
        parseddoc["x-summary"] = docstring.short_description
        parseddoc["x-deprecation"] = docstring.deprecation
        parseddoc["raises"] = {} # TODO
        parseddoc["properties"] = []
        parseddoc["required"] = []
        parseddoc["returns"] = None
                
        if docstring.returns:
            parseddoc["returns"] = {
                "type": "object", # default returns string
                "x-type": docstring.returns.type_name,
                "description": docstring.returns.description,
                "schema": lookup_schema(docstring.returns.type_name, schemas)
            } 
            if docstring.returns.type_name in schemas:
                parseddoc["returns"]["schema"] = docstring.returns.type_name

        parseddoc["returns"] = parse_extract_format(parseddoc["returns"])
        # save parameters
        for param in docstring.params:
            if not param.is_optional: parseddoc["required"].append(param.arg_name)
            param_dict = {
                "name": param.arg_name,
                "x-type": param.type_name,
                "type": param.type_name, # need to update this later (str > string, Xyst > "$ref": "#/definitions/xyst" )
                "description": param.description,
            }
            if param.default and not param.default == 'None':
                param_dict["default"] = param.default
                param_dict["examples"] = [param.default]
            param_dict = parse_extract_format(param_dict)
            parseddoc["properties"].append(param_dict)
    return parseddoc 

def load_docstring_bytype(module, type_name, schemas={}):
    """Extracts methods an all their docstrings

    Args:
        module (instance): python module instance
        type_name (str): is the type of the attribute we are extracting, must be one of these "<class 'function'>", 
    """
    methods = []
    for m in module.__dict__:
        if m.startswith("__"): continue # skipping hidden variables
        if not str(type(module.__dict__[m])) == type_name: continue # skipping anyting expect functions
        doc = ""
        try: doc = module.__dict__[m].__doc__
        except: pass
        docstring = parse_docstring(doc, schemas)
        methods.append({
            "method": m,
            "doc": doc,
            "line": 0, # TODO: calculate the line number,
            "docstring": docstring
        })
    return methods
"""
def load_pythonfile(file_path):
    module_name = file_path #file_name.replace("")
    spec = importlib.util.spec_from_file_location(module_name, file_path)
    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)    
    return {"module": module, "spec": spec, "path": file_path}
"""

def findall_docstring_sections(file_content):
    # all quote sections
    method_docstrings = []
    header_docstrings = ""
    # split document into function sections
    all_functions = file_content.split("def ")

    for an_function in all_functions:
        # find first occurance of tripple quoates ''' or """
        docstring_starts = an_function.find("\"\"\"") + 3
        # find next  occurance of tripple quoates ''' or """
        docstring_ends = an_function.find("\"\"\"", docstring_starts+1)
        # this is an header if it was the first occurance and has no : before start of string
        if len(method_docstrings) == 0 and an_function.find(":") > docstring_starts:
            header_docstrings = an_function[docstring_starts: docstring_ends]
        else:
            method_name = an_function[0:an_function.find("(")].strip()
            method_docstrings.append({"doc": an_function[docstring_starts: docstring_ends], "method_name": method_name}) 
    return {"header": header_docstrings, "methods": method_docstrings}

def load_docstring(file_path, schemas={}):
    """
    Loads module in memory extracting doc strings
    Returns an array of docstrings
    """
    
    #pyfile = load_pythonfile(file_path)
    #module = pyfile["module"]
    method_docstrings = []
    file_content = read_file(file_path)
    docstring_sections = findall_docstring_sections(file_content)
    # load the strings
    docstring = None

    for docstring_section in docstring_sections["methods"]:
        docstring = parse_docstring(docstring_section["doc"], schemas)
        method_docstrings.append({
            "method": docstring_section["method_name"],
            "doc": docstring_section["doc"],
            "line": file_content.find(docstring_section["doc"]), # TODO: calculate the line number,
            "docstring": docstring
        })    

    summary = ""
    """
    if len(module.__doc__):
        summary = module.__doc__.split("\n")[0]
        try:
            pass ##docstring = docstring_parser.parse(module.__doc__, style=docstring_parser.Style.google)
        except: pass        
    """
    return {
        "file": {
            "name": file_path,
            "description": docstring_sections["header"],
            "summary": summary,
            "content": read_file(file_path)
        },
        "header": docstring,
        #"classes": load_docstring_bytype(module, "<class 'class'>", schemas), # not supported right now
        "methods": method_docstrings #load_docstring_bytype(module, "<class 'function'>", schemas)
    }

def docstringmethod2jsonschema(schema_name, method_name, parsed_method_docstr, orginal_schema={}):
    """Parses the methods docstring returning json-schema

    Args:
        parsed_method_docstr (object): parsed docstring object

    Returns:
        JSON-schema for docstring

    """
    # hard data, not overwritten
    method_schema = {
        "$schema": "http://json-schema.org/draft-07/schema",
        "$id": "http://example.com/" + str(method_name).lower(), 
        "type": "object",
        "title": parsed_method_docstr["docstring"]["x-summary"],
        "description": parsed_method_docstr["docstring"]["description"],
        "default": {},
        "required": parsed_method_docstr["docstring"]["required"],
        "properties":{},
        "x-tags": [schema_name.lower().title()],
        "x-service": str(schema_name).lower(),
        "x-method": method_name,
        "x-deprecated": parsed_method_docstr["docstring"]["x-deprecation"]
    }

    # overwritable data
    if "x-oapi-method" in orginal_schema.keys():
        if not "path" in orginal_schema["x-oapi-method"]:
            orginal_schema["x-oapi-method"]["path"] = "/xyst/{xyst_id}" # TODO
        if not "method" in orginal_schema["x-oapi-method"]:
            orginal_schema["x-oapi-method"]["method"] = "post"
        if not "call" in orginal_schema["x-oapi-method"]:
            orginal_schema["x-oapi-method"]["call"] = "" # do not know this right now FIXME
        method_schema["x-oapi-method"] = {
            # "call": "getXyst",                # FIXME
            # "operation": method_name, SKIPPING THIS USING x-method instead
            "returns": parsed_method_docstr["docstring"]["returns"]
        }
    # add to tags if schema
    if parsed_method_docstr["docstring"]["returns"] and parsed_method_docstr["docstring"]["returns"]["schema"]:
        method_schema["x-tags"].append(parsed_method_docstr["docstring"]["returns"]["schema"].title())
    for prop in parsed_method_docstr["docstring"]["properties"]:
        prop["$id"] = "#/properties/" + str(method_name).lower() + "/" + str(prop["name"]).lower()
        method_schema["properties"][prop["name"]] = prop
    # update new data with old values
    method_schema = dict_of_dicts_merge(orginal_schema, method_schema)
    return method_schema

def docstring2jsonschema (schema_name, parsed_docstr, schemas={}):
    """
    Loads module in memory extracting doc strings
    Returns an array of docstrings
    """
    #file_schema = {
    #    "$schema": "http://json-schema.org/draft-07/schema",
    #    "$id": "http://example.com/" + schema_name + ".json", # TODO
    #    "type": "object",
    #    "title": parsed_docstr["file"]["summary"],
    #    "description": parsed_docstr["file"]["description"],
    #    "default": {},
    #    "required": [],
    #    "properties":{}
    #}
    #file_schema["required"] = []

    for docstringmethod in parsed_docstr["methods"]:
        method_name = schema_name + "-" + docstringmethod["method"].lower()
        # add to new schema
        method_schema = {}
        if method_name in schemas: method_schema = schemas[method_name]
        schemas[method_name] = docstringmethod2jsonschema(schema_name, method_name, docstringmethod, method_schema)
        # add to all schmea
        # NOTUSED: file_schema["properties"][method_name] = schemas[method_name]
    
    #NOTUSED: schemas["*"] = file_schema
    return schemas


##########################################################################################
## 
## Open API generator
## TODO: should be in another module
##########################################################################################
OpenAPIDefinitions = {
    "ErrorResult":{
        "description":"Standard error result",
        "type":"object",
        "properties":{
            "success":{
            "type":"boolean",
            "description":"Returns true on success and failure if something is wrong"
            },
            "error":{
            "type":"object",
            "description":"Error object",
            "properties":{
                "code":{
                    "type":"integer",
                    "description":"The error code"
                },
                "message":{
                    "type":"string",
                    "description":"The human readable error message"
                }
            }
            }
        }
    }
}
OpenAPIErrorResponse = {
    "description":"Standard error result",
    "schema":{
        "$ref":"#/definitions/ErrorResult"
    }
}

def defaultsToString(tpe):
    tpes = ["string", "number", "boolean", "integer", "array"]
    if tpe not in tpes: return "string"
    return tpe

def to_camelCase(text):
    camel_cased = ''.join(x for x in text.title() if not x.isspace())
    camel_cased = camel_cased.replace("-", "").replace("_", "")
    return camel_cased

def gd(dct, key, default=None):
    if dct == None or key == None: return default
    if key in dct: return dct[key]
    return default

def schema2openapi_pathparameters(schema):
    parameters = []
    for parameter_name in schema["properties"]:
        parameter = schema["properties"][parameter_name]
        parameters.append({
            "description": gd(parameter, "description", "TBD"),
            "in": gd(parameter, "x-oapi-param", "query"),
            "name": parameter_name,
            "type": defaultsToString(gd(parameter, "type", "object")),
            "required": parameter_name in schema["required"] 
        })
    return parameters

def convert2openapi_schema(json_schema):
    """Converts json-schema to openapi definition schema

    By making a copy and recursevely removing $id, $schema, examples, require

    Args:
        json_schema ([type]): [description]

    Returns:
        [type]: [description]
    """
    # TODO
    remove_keys = ["$id", "$schema", "examples", "required"]

    for attr in list(json_schema.keys()):
        if "properties" == attr:
            for prop in json_schema["properties"]:
                json_schema["properties"][prop] = convert2openapi_schema(json_schema["properties"][prop])        
        for rmkey in remove_keys:
            if attr == rmkey: del json_schema[attr]
    return json_schema

def schema2openapi_pathresponses(returns, schemas):
    response = {}
    definitions = {}
    if returns and returns["schema"] in schemas: # we have the schema that is returned
        schema = schemas[returns["schema"]]
        # name of the definition
        definition_name = returns["schema"] # default name
        if "title" in schema: # pretty name
            definition_name =  schema["title"].replace(" ", "").replace("_", "-").strip()
        # save to definitions
        definitions[definition_name] = convert2openapi_schema(schema)
        # save to response
        response["200"] = {
            "description": returns["description"],
            "schema": {
                "$ref":"#/definitions/" + definition_name
            }
        }
    else:
        response["200"] = {
            "description": "OK",
        }
    # save error handler
    response["default"] = OpenAPIErrorResponse
    response["500"] = OpenAPIErrorResponse

    return response, definitions

def schema2openapi_paths(schemas):
    paths = {}
    definitions = {}
    operations = {}
    for schema_name in schemas:
        schema = schemas[schema_name]
        # has this schema a method path
        if "x-oapi-method" in schema:
            responses, definition = schema2openapi_pathresponses(schema['x-oapi-method']['returns'], schemas)
            serviceName = to_camelCase(gd(schema, "x-service", schema_name))
            operationId = to_camelCase(gd(schema, "x-method", serviceName))
            # get the tags
            tags = list(definition.keys())
            tags.append(serviceName.title())
            # require.operations
            operations[operationId] = {
                "call": gd(schema['x-oapi-method'], 'call', 'TBD'), #  "call": "warehouse_state.getXyst"
                "schema": gd(schema, 'x-method', None),
                "returns": gd(schema['x-oapi-method']['returns'], 'schema', None)
            }
            # openapi.paths
            path_method = {
                "operationId": operationId, 
                "tags": tags,
                "parameters": schema2openapi_pathparameters(schema),
                "responses": responses
            }
            path = {
                "x-name": to_camelCase(re.sub('{.*?}', '', gd(schema['x-oapi-method'], 'path', serviceName).replace("/", " ")).strip())
            }
            path[gd(schema["x-oapi-method"], "method", "get")] = path_method
            # merge reacurring paths methods with already registered paths. Example /name/{name} and /name/{name} in different schema files
            paths = dict_of_dicts_merge(paths, {gd(schema["x-oapi-method"], "path", "/"): path})
            # collect definition
            definitions = dict_of_dicts_merge(definitions, definition)
    return paths, definitions, operations

def schema2makerschema(schemas, openapi={}):
    # generate paths
    paths, definitions, operations = schema2openapi_paths(schemas)
    openapi["paths"] = paths
    # generate definitions
    openapi["definitions"] = dict_of_dicts_merge(OpenAPIDefinitions, definitions)

    return {
        "openapi": openapi, 
        "require":{
            "operation": operations
        }
    }




##########################################################################################
## 
## Merge file
## 
##########################################################################################

def mergerequiredfile (makeschema, docstr):
    if not "require" in makeschema: makeschema["require"] = {}
    if not "import" in makeschema["require"]: makeschema["require"]["import"] = {}
    if not "library" in makeschema["require"] :makeschema["require"]["import"]["library"] = []
    # merge the file for this docstring
    makeschema["require"]["import"]["code"] = docstr["file"]["content"]
    return makeschema

##########################################################################################
## 
## File : TODO: should be in another module
## 
##########################################################################################

def parse_directory(directory, schemas={}):
    files = load_directory(directory)
    file_schemas = {}
    for file_path in files:
        file_name = os.path.basename(file_path)
        if file_name.startswith("__"): continue
        logging.info("Parsing " + str(file_name))
        schema_name = file_name.replace(".py", "")
        docstr = load_docstring(file_path, schemas)
        # method input schema
        jsnschema = docstring2jsonschema(schema_name, docstr)        
        file_schemas[schema_name] = jsnschema
    return file_schemas


def write_schemas_directory(file_schemas, schema_directory = "./schema/"):
    # write files to schema directory
    for file_schema in file_schemas:
        for file_method in file_schemas[file_schema]:
            schema_file_path = schema_directory + file_method + ".json"
            print("Writing schema  " + str(schema_file_path))
            #print(str(file_schemas[file_schema][file_method]))
            write_json(schema_file_path, file_schemas[file_schema][file_method])


def load_json_directory(schema_directory = "./schema/"):
    # read files from schema directory
    schemas = {}
    files = load_directory(schema_directory, "json")
    for file_path in files:
        file_name = os.path.basename(file_path).replace(".json", "")
        schemas[file_name] = read_json(file_path)    

    return schemas




import json
import os

# file 

def write_file(path, content, fail_if_missing_directory=False):
    if not fail_if_missing_directory:
        if path.find(".") != -1 and path.find("/") == -1: path = "./" + path
        os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as file:  
        try:
            file.write(content)
        except Exception as e:
            print("Error writing file " + str(path) + " content " + str(file))

def read_file(path):
    with open(path, 'r') as file:
        return file.read()


# json

def read_json(path):
    with open(path, 'r') as file:  
        return json.loads(file.read())

def write_json(path, dict_data):
    json_data = dict_data
    if type (dict_data) == dict:
        json_data = json.dumps(dict_data, indent=4, sort_keys=True)
    return write_file(path, json_data)

def dict_of_dicts_merge(x, y):
    # https://stackoverflow.com/questions/38987/how-do-i-merge-two-dictionaries-in-a-single-expression-in-python-taking-union-o
    z = {}
    overlapping_keys = x.keys() & y.keys()
    for key in overlapping_keys:
        if type(x[key]) == dict and type(y[key]) == dict:
            z[key] = dict_of_dicts_merge(x[key], y[key])
        else:
            if key in x: z[key] = x[key] # overwrite
            if key in y: z[key] = y[key] # default
    for key in x.keys() - overlapping_keys:
        z[key] = deepcopy(x[key])
    for key in y.keys() - overlapping_keys:
        z[key] = deepcopy(y[key])
    return z