"""Console script for pydoc2schema."""
import sys
import click

import pydoc2schema



import json, os

# file 

def write_file(path, content, fail_if_missing_directory=False):
    if not fail_if_missing_directory and path:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as file:  
        file.write(content)

def read_file(path):
    with open(path, 'r') as file:  
        return file.read()


# json

def read_json(path):
    with open(path, 'r') as file:  
        return json.loads(file.read())

def write_json(path, dict_data):
    json_data = dict_data
    if type (dict_data) == dict:
        json_data = json.dumps(dict_data, indent=4, sort_keys=True)
    return write_file(path, json_data)



@click.command()
@click.option('--schema_directory', default="./schema/", help='The schema directory that we are working with')
@click.option('--project_directory', prompt='Project folder or file',
              help='The python project having all the python docstrings to parse')

def main(schema_directory, project_directory):
    """Console script for pydoc2schema."""
    click.echo("Generate schema from pydocs")
    # TODO: load schemas
    # parse all python files
    file_schemas = pydoc2schema.parse_directory(project_directory, pydoc2schema.load_json_directory(schema_directory))
    pydoc2schema.write_schemas_directory(file_schemas)
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
