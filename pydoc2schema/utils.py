import json
import os

# file 

def write_file(path, content, fail_if_missing_directory=False):
    if not fail_if_missing_directory:
        os.makedirs(os.path.dirname(path), exist_ok=True)
    with open(path, 'w') as file:  
        file.write(content)

def read_file(path):
    with open(path, 'r') as file:  
        return file.read()


# json

def read_json(path):
    with open(path, 'r') as file:  
        return json.loads(file.read())

def write_json(path, dict_data):
    json_data = dict_data
    if type (dict_data) == dict:
        json_data = json.dumps(dict_data, indent=4, sort_keys=True)
    return write_file(path, json_data)
