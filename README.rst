============
pydoc2schema
============


.. image:: https://img.shields.io/pypi/v/pydoc2schema.svg
        :target: https://pypi.python.org/pypi/pydoc2schema

.. image:: https://img.shields.io/travis/kajjjak/pydoc2schema.svg
        :target: https://travis-ci.com/kajjjak/pydoc2schema

.. image:: https://readthedocs.org/projects/pydoc2schema/badge/?version=latest
        :target: https://pydoc2schema.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/kajjjak/pydoc2schema/shield.svg
     :target: https://pyup.io/repos/github/kajjjak/pydoc2schema/
     :alt: Updates



Reads python files in directory and generates json-schema


* Free software: BSD license
* Documentation: https://pydoc2schema.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
