#!/usr/bin/env python

"""Tests for `pydoc2schema` package."""

import unittest
import os
import json

from pydoc2schema import pydoc2schema
from pydoc2schema import cli

class TestMainModule(unittest.TestCase):

    def setUp(self):
        self.working_directory = os.path.dirname(os.path.realpath(__file__)) + "/../"

    def test_extractingdocstring(self):
        """
        Should extract docstrings from module
        """
        docstr = pydoc2schema.load_docstring ( self.working_directory + "test/data/warehouse/warehouse.py")
        # method input schema
        jsnschema = pydoc2schema.docstring2jsonschema("warehouse", docstr)
        # TODO: method returns schema
        self.assertEqual(len(jsnschema), 3)

    def test_parsedirectorydocstrings(self):
        """
        Should extract docstrings from directory files
        """
        files = pydoc2schema.parse_directory ( self.working_directory + "test/data/warehouse/warehouse.py")
        # method input schema
        self.assertEqual(len(files["warehouse"]), 3)


    def test_writingschemadirectory(self):
        """
        Should write files to directory
        """        
        file_schemas = pydoc2schema.parse_directory(self.working_directory + "test/data/warehouse/warehouse.py")
        pydoc2schema.write_schemas_directory(file_schemas, self.working_directory + "test/data/schema/")        

    def test_readschemadirectory(self):
        """
        Should write files to directory
        """        
        file_schemas = pydoc2schema.load_json_directory(self.working_directory + "test/data/schema/")
        pydoc2schema.write_schemas_directory({"warehouse": file_schemas}, self.working_directory + "test/data/schema/")  

    def test_overwritingschema(self):
        """
        Should write files to directory
        """        
        docstr = pydoc2schema.load_docstring ( self.working_directory + "test/data/warehouse/warehouse.py")
        file_schemas = pydoc2schema.load_json_directory(self.working_directory + "test/data/schema/")
        # do change to schema
        file_schemas["warehouse-getxyst"] = {
            "title": "OVERRIDEN",
            "type": "OVERRIDEN",
            "description": "OVERRIDEN",
            "properties": {
                "xyst_id": {
                    "$id":"OVERRIDEN",
                    "description": "OVERRIDEN",
                    "name": "OVERRIDEN",
                    "type": "OVERRIDEN",
                    "x-oapi-param": "path"
                }
            },
            "required": [],
            "x-oapi-method": {
                "path": "/xyst/{xyst_id}",
                "call": "SAVED",
                "method": "SAVED",
                "returns": "OVERRIDEN"
            }  
        }        
        jsnschema = pydoc2schema.docstring2jsonschema("warehouse", docstr, file_schemas)
        self.assertEqual(len(jsnschema), 5)
        file_schema_str = json.dumps(jsnschema)
        self.assertTrue("x-oapi-method" in jsnschema["warehouse-getxyst"])
        oapi_method = jsnschema["warehouse-getxyst"]["x-oapi-method"]
        self.assertEqual(oapi_method["path"], "/xyst/{xyst_id}")
        self.assertEqual(oapi_method["returns"]["type"], "object")
        self.assertEqual(oapi_method["returns"]["x-type"], "xyst-schema")
        self.assertEqual(oapi_method["returns"]["description"], "Xyst schema")
        self.assertEqual(oapi_method["returns"]["schema"], None)
        self.assertEqual(file_schema_str.count("OVERRIDEN"), 0)
        self.assertEqual(file_schema_str.count("SAVED"), 2)



class TestOpenAPI(unittest.TestCase):

    openapi = {
        "swagger":"2.0",
        "info":{
            "title":"Warehouse",
            "description":"List of volcanic eruptions based on",
            "version":"0.1.0"
        },
        "host":"localhost:8080",
        "schemes":[
            "https",
            "http"
        ],
        "basePath":"/warehouse/v1",
        "produces":[
            "application/json",
            "text/json"
        ]
    }

    def setUp(self):
        self.working_directory = os.path.dirname(os.path.realpath(__file__)) + "/../"

    def test_generatingpathfromschema(self):
        """
        Should extract docstrings from module
        """
        file_schemas = pydoc2schema.load_json_directory(self.working_directory + "schema/")
        docstr = pydoc2schema.load_docstring ( self.working_directory + "test/data/warehouse/warehouse.py", file_schemas)
        jsnschema = pydoc2schema.docstring2jsonschema("warehouse", docstr, file_schemas)
        makerschema = pydoc2schema.schema2makerschema(jsnschema, self.openapi)
        # pydoc2schema.write_json("warehouse-api-v2.json", makerschema)
        self.assertEqual(1,1)


    def test_generaterequireimport(self):
        """
        Should create import statement
        TODO
        """
        file_schemas = pydoc2schema.load_json_directory(self.working_directory + "schema/")
        docstr = pydoc2schema.load_docstring ( self.working_directory + "test/data/warehouse/warehouse.py", file_schemas)
        jsnschema = pydoc2schema.docstring2jsonschema("warehouse", docstr, file_schemas)
        makerschema = pydoc2schema.schema2makerschema(jsnschema, self.openapi)
        makerschema = pydoc2schema.mergerequiredfile(makerschema, docstr)
        pydoc2schema.write_json("warehouse-api-v2.json", makerschema)
        self.assertTrue("import" in makerschema["require"])
        # library
        self.assertTrue("library" in makerschema["require"]["import"])
        self.assertTrue(len(makerschema["require"]["import"]["library"]) >= 0)
        # code
        self.assertTrue("code" in makerschema["require"]["import"])
        self.assertTrue(len(makerschema["require"]["import"]["code"]) > 1000) # library contains at least 1000 lines of code
