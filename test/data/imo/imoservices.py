import json
import datetime
from decimal import Decimal
from sqlalchemy.sql import text
from sqlalchemy import bindparam 
from sqlalchemy import String, DateTime, Boolean, Integer, Numeric
from sqlalchemy.exc import ArgumentError
#from flask_sqlalchemy import SQLAlchemy # pip install flask_sqlalchemy --user
from sqlalchemy import create_engine
POSTGRES = {
    'user': 'kjartan',
    'pw': 'superman',
    'db': 'Stafli', #'kjartan_test',
    'host': 'stafli.db.vedur.is', #'dev1.psql.vedur.is',
    'port': '50010', #'5432',
    'conn': 'db2+ibm_db' # 'postgresql'
}
# psql install
#dbconnection = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
# https://github.com/ibmdb/python-ibmdbsa/tree/master/ibm_db_sa
dbconnection = '%(conn)s://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
#app.config['DEBUG'] = True
#app.config['SQLALCHEMY_DATABASE_URI'] = dbconnection
#db = SQLAlchemy()
#db.init_app(server.app)
engine = create_engine(dbconnection, echo=True)


class JSONEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime.datetime):
            return (str(z))
        elif isinstance(z, Decimal):
            return (float(z))
        else:
            return super().default(z)

def _toJSON(data):
    return json.dumps(data, cls=JSONEncoder, indent=4, sort_keys=True)  

def _query(sql_cmd, commit):
    resultproxy = engine.execute(sql_cmd)

    d, data = {}, []
    for rowproxy in resultproxy:
        # rowproxy.items() returns an array like [(key0, value0), (key1, value1)]
        for tup in rowproxy.items():
            # build up the dictionary
            d = {**d, **{tup[0]: tup[1]}}
        data.append(d)

    if commit:
        # Commit detected by template
        try:
            db.session.commit()
        except Exception as e: 
            return None
    else:
        return data



def callAvalanceEvacuation():
    """AvalanceEvacuation
    Active evacuation
    
    Returns:
        AvalanceEvacuation: Active evacuation
    """
    sql_str = """
SELECT
  rm.stadur,
  st.nafn,
  rm.sett,
  rm.hvadrymt
FROM oli.ryming rm,
     oli.stadur st
WHERE rm.stadur = st.stadur
AND aflyst IS NULL
ORDER BY st.radnr
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callAvalanceNotice(startime, endtime):
    """AvalanceNotice
    Avalance notice by date
    Args:
        startime (date): Period begin. Example 2019-10-10
        endtime (date): Period ends. Example 2019-10-30
    
    Returns:
        AvalanceNotice: Avalance notice by date
    """
    sql_str = """
SELECT
  FLOD,
  TIMI,
  ATHS_DAGS,
  ATHS_DATE,
  PM_DAGUR,
  ATHS_KLUKKA,
  PM_KLUKKA,
  FARVEGUR,
  FARVEGARNAFN,
  SVAEDI,
  SVAEDISNAFN,
  STADUR,
  STADARNAFN,
  VEFSKRLANDSHLUTI,
  ATHS_STADS,
  STADSETNINGFLODSINS,
  TEG_FLOD,
  TEG_LYSING,
  ORSOK_LYSING,
  TJON,
  ATHS_STUTT,
  ATHS_SHORT,
  ATHS_VEF,
  ATHS_WEB,
  (CASE
    WHEN xyf.TILBUID = 'T' THEN 1
    ELSE 0
  END) AS tilbuid,
  (CASE
    WHEN xyf.FRAGENGID = 'T' THEN 1
    ELSE 0
  END) AS fragengid,
  dec(ROUND(COALESCE(xyf.ORSOK, -99), 1), 3, 1) AS orsok,
  dec(ROUND(COALESCE(xyf.STAERD, -99), 1), 3, 1) AS staerd,
  (CASE
    WHEN xyf.AFMANNAVOLDUM = 'T' THEN 1
    ELSE 0
  END) AS afmannavoldum,
  (CASE
    WHEN xyf.LOKADI_VEGI = 'T' THEN 1
    ELSE 0
  END) AS lokadi_vegi,
  (CASE
    WHEN xyf.TEG_FARVEGUR IS NULL THEN -99
    ELSE xyf.TEG_FARVEGUR
  END) AS teg_farvegur,
  (CASE
    WHEN xyf.FJ_FORST IS NULL THEN 0
    ELSE xyf.FJ_FORST
  END) AS fj_forst,
  (CASE
    WHEN xyf.FJ_SLAS IS NULL THEN 0
    ELSE xyf.FJ_SLAS
  END) AS fj_slas,
  (CASE
    WHEN xyf.FJ_OMEIDD IS NULL THEN 0
    ELSE xyf.FJ_OMEIDD
  END) AS fj_omeidd,
  (CASE
    WHEN xyf.LENGD IS NULL THEN -99.0
    ELSE xyf.LENGD
  END) AS lengd,
  (CASE
    WHEN xyf.BREIDD IS NULL THEN -99.0
    ELSE xyf.BREIDD
  END) AS breidd
FROM kort.oli.flodxyf AS xyf
WHERE timi >= :starttime
AND timi <= :endtime
AND teg_flod NOT IN (10, 11, 12, 13)
ORDER BY timi DESC, staerd DESC
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callAvalancePreparedness():
    """AvalancePreparedness
    Avalance preparedness
    
    Returns:
        AvalancePreparedness: Avalance preparedness
    """
    sql_str = """
SELECT
  va.vbalandshluti,
  lh.nafn,
  va.sett,
  va.aths
FROM oli.vidbastand va,
     oli.vbalandshluti lh
WHERE va.vbalandshluti =
lh.vbalandshluti
AND va.aflyst IS NULL
ORDER BY lh.radnr
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callGenericWarning(title, author):
    """GenericWarning
     
    Args:
        title (string): The title of the book.
        author (string): The author of the book
    
    Returns:
        GenericWarning:  
    """
    sql_str = """
SELECT 
  warn.id, 
  title_is.title AS title_is, 
  warn.text_is AS text_is, 
  warn.link_is AS link_is, 
  warn.not_used_is AS not_used_is, 
  title_en.title AS title_en, 
  warn.text_en AS text_en, 
  warn.link_en AS link_en, 
  warn.not_used_en AS not_used_en 
FROM 
  dskra.web_warnings_warning AS warn 
  LEFT JOIN dskra.web_warnings_warningtitle AS title_is ON warn.title_is_id = title_is.id 
  LEFT JOIN dskra.web_warnings_warningtitle AS title_en ON warn.title_en_id = title_en.id 
ORDER BY 
  warn.id
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('title', type_=String),
            bindparam('author', type_=String)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callAvalSpecialistComment():
    """RegistrationAvalSpecialistComment
    Specialist comment notice by date
    
    Returns:
        AvalSpecialistComment: Specialist comment notice by date
    """
    sql_str = """
SELECT * FROM dskra.aval_notes_comment
        WHERE valid_until IS null OR valid_until > CURRENT TIMESTAMP
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callGeoSpecialistComment():
    """RegistrationGeoSpecialistComment
    Active geo specialist comment
    
    Returns:
        GeoSpecialistComment: Active geo specialist comment
    """
    sql_str = """
SELECT * FROM dskra.geo_notes_comment
		WHERE valid_until IS null OR valid_until > CURRENT TIMESTAMP
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callHydrologySpecialistComment():
    """RegistrationHydrologySpecialistComment
    Active Hydrology Specialist comment
    
    Returns:
        HydrologySpecialistComment: Active Hydrology Specialist comment
    """
    sql_str = """
SELECT * FROM dskra.hydrology_notes_comment
		WHERE valid_until IS null OR valid_until > CURRENT TIMESTAMP
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSeaiceNavtexNotice(startime, endtime):
    """SeaiceNavtexNotice
    Seaice reports
    Args:
        startime (datetime): Period begin. Example 2019-01-01
        endtime (datetime): Period ends. Example 2019-01-30
    
    Returns:
        SeaiceNavtexNotice: Seaice reports
    """
    sql_str = """
SELECT 
  ID, TIMI, KOMUTIMI, SKEYTI
FROM 
  hafis.stil31 
WHERE 
  timi >= :starttime AND 
  timi <= :endtime  
ORDER BY timi DESC
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSeaiceNotice(startime, endtime):
    """SeaiceNoice
    Seaice notice
    Args:
        startime (date): Period begin
        endtime (date): Period ends
    
    Returns:
        SeaiceNotice: Seaice notice
    """
    sql_str = """
SELECT 
  ice.ID, 
  ice.USER_ID, 
  ice.REG_TIME, 
  ice.OBS_TIME, 
  ice.OBS_TYPE_ID, 
  ice.SEAICE_DESC, 
  ice.OBS_NAME, 
  ice.OBS_PHONE, 
  ice.FURTHER_INFO, 
  ice.SINGLE_LATLON_FORMAT, 
  ice.SINGLE_ICE_POS, 
  ice.EDGE_LATLON_FORMAT, 
  ice.EDGE_ICE_POS, 
  ice.PUBLISH_STATUS, 
  user.FIRST_NAME AS REG_FIRST_NAME, 
  user.LAST_NAME AS REG_LAST_NAME, 
  type.TYPE AS OBS_TYPE 
FROM 
  dskra.sea_ice_seaice AS ice, 
  dskra.auth_user AS user, 
  dskra.sea_ice_seaiceobstype AS type 
WHERE 
  obs_time >= :starttime 
  AND obs_time <= :endtime 
  AND publish_status = 'p' 
  AND ice.user_id = user.id 
  AND ice.obs_type_id = type.id 
ORDER BY 
  obs_time DESC
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSeaiceNoticeImage(notice_id_list):
    """SeaiceNoiceImage
    Seaice Notice Image
    Args:
        notice_id_list (list): List of valid notice_id
    
    Returns:
        SeaiceNoticeImage: Seaice Notice Image
    """
    sql_str = """
SELECT 
  id, 
  seaice_registration_id AS notice_id, 
  photo_caption, 
  is_map, 
  image 
FROM 
  dskra.sea_ice_seaicephoto 
WHERE 
  seaice_registration_id IN (:notice_id_list)
    ORDER BY 
      seaice_registration_id,  
      is_map DESC, 
      id
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSeaiceNotice(startime, endtime):
    """SeaiceNotice
    Seaice notice
    Args:
        startime (date): Period begin
        endtime (date): Period ends
    
    Returns:
        SeaiceNotice: Seaice notice
    """
    sql_str = """
SELECT 
  ice.ID, 
  ice.USER_ID, 
  ice.REG_TIME, 
  ice.OBS_TIME, 
  ice.OBS_TYPE_ID, 
  --ice.SEAICE_DESC AS SEAICE_DESC_CLOB,
  CAST(ice.SEAICE_DESC AS VARCHAR(20000)) AS SEAICE_DESC,
  ice.OBS_NAME, 
  ice.OBS_PHONE, 
  -- ice.FURTHER_INFO AS FURTHER_INFO_CLOB,
  CAST(ice.FURTHER_INFO AS VARCHAR(50000)) AS FURTHER_INFO,
  ice.SINGLE_LATLON_FORMAT, 
  -- ice.SINGLE_ICE_POS AS SINGLE_ICE_POS_CLOB, 
  CAST(ice.SINGLE_ICE_POS AS VARCHAR(15000)) AS SINGLE_ICE_POS,
  ice.EDGE_LATLON_FORMAT, 
  -- ice.EDGE_ICE_POS AS EDGE_ICE_POS_CLOB, 
  CAST(ice.EDGE_ICE_POS AS VARCHAR(20000)) AS EDGE_ICE_POS,
  ice.PUBLISH_STATUS, 
  user.FIRST_NAME AS REG_FIRST_NAME, 
  user.LAST_NAME AS REG_LAST_NAME, 
  type.TYPE AS OBS_TYPE 
FROM 
  dskra.sea_ice_seaice AS ice, 
  dskra.auth_user AS user, 
  dskra.sea_ice_seaiceobstype AS type 
WHERE 
  obs_time >= :starttime 
  AND obs_time <= :endtime 
  AND publish_status = 'p' 
  AND ice.user_id = user.id 
  AND ice.obs_type_id = type.id 
ORDER BY 
  obs_time DESC
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSeaiceNoticeImage(notice_id_list):
    """SeaiceNoticeImage
    Seaice Notice Image
    Args:
        notice_id_list (list): List of valid notice_id. Example string=32 or literal=32,33
    
    Returns:
        SeaiceNoticeImage: Seaice Notice Image
    """
    sql_str = """
SELECT 
  id, 
  seaice_registration_id AS notice_id, 
  --photo_caption AS photo_caption_CLOB, 
  CAST(photo_caption AS VARCHAR(20000)) AS photo_caption,
  is_map, 
  image 
FROM 
  dskra.sea_ice_seaicephoto 
WHERE 
  seaice_registration_id IN (:notice_id_list)
    ORDER BY 
      seaice_registration_id,  
      is_map DESC, 
      id
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callWeatherFixedObservervation(startime, endtime, stations):
    """WeatherFixedObservations
    Weather fixed observervation
    Args:
        startime (date): Period begin. Example 2019-10-01
        endtime (date): Period ends. Example 2019-10-02
        stations (integers): List of observation stations id. Example 1,2,3,4,5,6,7,8
    
    Returns:
        WeatherFixedObservervation: Weather fixed observervation
    """
    sql_str = """
SELECT 
  DISTINCT b.stod, 
  a.timi, 
  a.ar, 
  a.man, 
  a.dagur, 
  DEC(
    round(
      COALESCE(
        (CASE a.d WHEN 0 THEN 360 ELSE a.d END), 
        -99
      ), 
      1
    ), 
    5, 
    1
  ) AS d, 
  DEC(
    round(
      COALESCE(a.f,-99), 
      1
    ), 
    3, 
    1
  ) AS f, 
  DEC(
    round(
      COALESCE(a.fx,-99), 
      1
    ), 
    5, 
    1
  ) AS fx, 
  -99 AS fg, 
  DEC(
    round(
      COALESCE(a.t,-99), 
      1
    ), 
    5, 
    1
  ) AS t, 
  DEC(
    round(
      COALESCE(a.td,-99), 
      1
    ), 
    5, 
    1
  ) AS td, 
  DEC(
    round(
      COALESCE(a.tx,-99), 
      1
    ), 
    5, 
    1
  ) AS tx, 
  DEC(
    round(
      COALESCE(a.tn,-99), 
      1
    ), 
    5, 
    1
  ) AS tn, 
  -99 AS veghiti, 
  DEC(
    round(
      COALESCE(a.p,-99), 
      1
    ), 
    6, 
    1
  ) AS p, 
  DEC(
    round(
      COALESCE(a.pbr,-99), 
      1
    ), 
    5, 
    1
  ) AS pbr, 
  -99 AS rh, 
  -99 AS snd, 
  -99 AS snc, 
  (
    CASE WHEN a.s IS NULL THEN '-99' ELSE a.s END
  ) AS sjolag, 
  COALESCE(
    (
      CASE WHEN a.n = '/' THEN -99 ELSE CAST(a.n AS INTEGER) END
    ), 
    -99
  ) as skyjahula, 
  (
    CASE when a.w IS NULL THEN '-99' ELSE a.w END
  ) AS vedur, 
  w.vm AS vedurmerki, 
  '-99.0' AS skyggni, 
  DEC(
    round(
      COALESCE(a.r,-99), 
      1
    ), 
    5, 
    1
  ) as r, 
  (
    CASE WHEN a.r IS NULL THEN -99 ELSE (
      CASE WHEN HOUR(a.timi)= 9 THEN 900 ELSE 540 END
    ) END
  ) AS rminutes 
FROM 
  ath.gts a, 
  sta.stod b, 
  sta.vedurmerki_w_n AS w -- FIXME < isNotNull property = "stations" > 
WHERE 
    b.stod IN (:stations) 
    -- FIXME < iterate property = "stations" open = "" conjunction = "," close = "" > #stations[]#</iterate>) and </isNotNull>
    AND b.stod_wmo = a.stod_wmo 
    AND w.w = COALESCE(a.w, '//') 
    AND w.n = a.n 
    AND timi >= :starttime AND timi <= :endtime
    ORDER BY 
      1, 
      2
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime),
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callGenericWarning(title, author):
    """WeatherGenericWarning
     
    Args:
        title (string): The title of the book.
        author (string): The author of the book
    
    Returns:
        GenericWarning:  
    """
    sql_str = """
SELECT 
  warn.id, 
  title_is.title AS title_is, 
  warn.text_is AS text_is, 
  warn.link_is AS link_is, 
  warn.not_used_is AS not_used_is, 
  title_en.title AS title_en, 
  warn.text_en AS text_en, 
  warn.link_en AS link_en, 
  warn.not_used_en AS not_used_en 
FROM 
  dskra.web_warnings_warning AS warn 
  LEFT JOIN dskra.web_warnings_warningtitle AS title_is ON warn.title_is_id = title_is.id 
  LEFT JOIN dskra.web_warnings_warningtitle AS title_en ON warn.title_en_id = title_en.id 
ORDER BY 
  warn.id
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('title', type_=String),
            bindparam('author', type_=String)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callWeatherObservationTime(stations):
    """WeatherObservationTime
    Weather fixed observervation
    Args:
        stations (integers): List of observation stations id. Example 1,2,3,4,5,6,7,8
    
    Returns:
        WeatherObservationTime: Weather fixed observervation
    """
    sql_str = """
SELECT 
  stod, 
  MAX(timi) AS timi 
FROM 
  ath.ath 
WHERE 
  stod IN (:stations)
    -- FIXME < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
        GROUP BY stod
    UNION 
    SELECT 
      stod, 
      MAX(timi) AS timi 
    FROM 
      ath.sj_klst 
    WHERE 
      stod IN (:stations) 
        -- FIXME < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
            GROUP BY stod
        UNION
        SELECT 
          stod, 
          MAX(timi) AS timi 
        FROM 
          ath.sj_vg 
        WHERE 
          stod IN (:stations)
            -- FIXME < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
                GROUP BY stod
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callWeatherObservationTime(stations):
    """WeatherObservationTime10days
    Weather fixed observervation
    Args:
        stations (integers): List of observation stations id. Example 1,2,3,4,5,6,7,8
    
    Returns:
        WeatherObservationTime: Weather fixed observervation
    """
    sql_str = """
SELECT 
  stod, 
  MAX(timi) AS timi 
FROM 
  ath.ath 
WHERE 
  stod IN (:stations)
    -- < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>) 
    AND timi > CURRENT TIMESTAMP - 10 days 
    GROUP BY
      stod 
    UNION 
    SELECT 
      stod, 
      MAX(timi) AS timi 
    FROM 
      ath.sj_klst 
    WHERE 
      stod IN (:stations)
        -- FIXME < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
        AND timi > CURRENT TIMESTAMP - 10 days 
        GROUP BY
          stod 
        UNION 
        SELECT 
          stod, 
          MAX(timi) AS  timi 
        FROM 
          ath.sj_vg 
        WHERE
          stod IN (:stations)
            -- FIXME < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
            AND timi > CURRENT TIMESTAMP - 10 days 
            GROUP BY 
              stod
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callWeatherObservationTime10Minute():
    """WeatherObservationTime10minutes
    Weather fixed observervation every 10 minutes
    
    Returns:
        WeatherObservationTime10Minute: Weather fixed observervation every 10 minutes
    """
    sql_str = """
SELECT nafn                        AS stod,
       timi,
       wind,
       Dec(Round(f, 0), 3, 0)      AS vindur,
       Dec(Round(fg, 0), 3, 0)     AS max_hvida,
       Dec(Round(t, 1), 3, 1)      AS hiti,
       Dec(Round(tn, 1), 3, 1)     AS min_hiti,
       Dec(Round(tx, 1), 3, 1)     AS max_hiti,
       CASE
         WHEN rh > 100 THEN 100
         ELSE Dec(Round(rh, 0), 3, 0)
       END                         AS rakastig,
       CASE
         WHEN prump < 0.0 THEN 0
         ELSE prump
       END                         AS r10min,
       heiti                       AS svaedi,
       Dec(Round(h_stod, 0), 3, 0) AS h_stod
FROM   (SELECT s.nafn,
               a.timi,
               vind.att                  AS wind,
               a.f,
               a.fg,
               a.t,
               a.tn,
               a.tx,
               a.rh,
               a.r,
               sv.heiti,
               s.h_stod,
               ROW_NUMBER()
                 OVER (
                   PARTITION BY a.stod
                   ORDER BY a.timi desc) AS rn,
               a.R-lead(r, 1)
                 OVER (
                   PARTITION BY a.stod
                   ORDER BY a.timi desc) AS prump
        FROM   ath.sj_hragogn a,
               sta.stod s,
               sta.spa_svaedi sv,
               vefur.stodatridi vef,
               sta.vindatt vind
        WHERE  a.timi > CURRENT TIMESTAMP - 2 HOUR
               AND a.stod IN (SELECT stod
                              FROM   vefur.stodatridi
                              WHERE  stod > 999
                                     AND teg > 1)
               AND a.stod = s.stod
               AND vef.stod = a.stod
               AND vind.d = a.d
               AND s.spasv = sv.spasv
        ORDER  BY s.nafn,
                  a.timi asc)
WHERE  rn = 1;
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callObservationsResult(stations, startime, endtime):
    """WeatherObservations
    Weather Observations
    Args:
        stations (literal): Stations list. Example 1,2,3,4,5,6
        startime (date): Period begin. Example 2019-10-10
        endtime (date): Period ends. Example 2019-10-01
    
    Returns:
        ObservationsResult: Weather Observations
    """
    sql_str = """
SELECT
  DISTINCT a.stod AS stod,
  a.timi,
  a.ar,
  a.man,
  a.dagur,
  DEC(
    ROUND(
      COALESCE((CASE a.d WHEN 0 THEN 360 ELSE a.d END), -99),
      1
    ),
    5,
    1
  ) AS d,
  DEC(ROUND(COALESCE(a.f, -99), 1), 3, 1) AS f,
  DEC(ROUND(COALESCE(a.fx, -99), 1), 5, 1) AS fx,
  DEC(ROUND(COALESCE(a.fg, -99), 1), 5, 1) AS fg,
  DEC(ROUND(COALESCE(a.t, -99), 1), 5, 1) AS t,
  DEC(ROUND(COALESCE(a.td, -99), 1), 5, 1) AS td,
  DEC(ROUND(COALESCE(a.tx, -99), 1), 5, 1) AS tx,
  DEC(ROUND(COALESCE(a.tn, -99), 1), 5, 1) AS tn,
  -99 AS veghiti,
  DEC(ROUND(COALESCE(a.p, -99), 1), 6, 1) AS p,
  DEC(ROUND(COALESCE(a.pbr, -99), 1), 5, 1) AS pbr,
  DEC(ROUND(COALESCE(a.rh, -99), 1), 5, 1) AS rh,
  DEC(ROUND(COALESCE(a.snd, -99), 1), 5, 1) AS snd,
  (CASE WHEN a.snc is null THEN -99 ELSE a.snc END) AS snc,
  (CASE WHEN a.s is null THEN -99 ELSE a.s END) AS sjolag,
  (CASE WHEN a.n is null THEN -99 ELSE a.n END) AS skyjahula,
  (CASE WHEN a.w is null THEN -99 ELSE a.w END) AS vedur,
  w.vm AS vedurmerki,
  k.skyggni_i_km AS skyggni,
  DEC(ROUND(COALESCE(a.r, -99), 1), 5, 1) AS r,
  (
    CASE WHEN a.r is null THEN -99 else (CASE WHEN HOUR(a.timi) = 9 THEN 900 else 540 END) END
  ) AS rminutes
FROM
  ath.ath a,
  sta.vedurmerki_w_n AS w,
  sta.skyjahula AS n,
  sta.skyggni AS k --
  -- WHERE stod IN (1,2,3,4,5,6,7,8,9) and
WHERE
  stod IN (:stations) -- FIXME <isNotNull property="stations"> WHERE stod IN (<iterate property="stations" open="" conjunction="," close="">#stations[]#</iterate>) AND </isNotNull>
  AND w.w = substr(CAST(CAST(a.w AS decimal(2, 0)) AS CHAR(3)), 1, 2)
  AND w.n = CAST(a.n AS CHAR(1))
  AND n.n = CAST(a.n AS CHAR(1))
  AND CAST(k.v AS INTEGER) = CAST(a.v AS INTEGER)
  AND timi >= :starttime
  AND timi <= :endtime
UNION
SELECT
  DISTINCT a.stod AS stod,
  a.timi,
  a.ar,
  a.man,
  a.dagur,
  DEC(
    ROUND(
      COALESCE((CASE a.d WHEN 0 THEN 360 ELSE a.d END), -99),
      1
    ),
    5,
    1
  ) AS d,
  DEC(ROUND(COALESCE(a.f, -99), 1), 5, 1) AS f,
  DEC(ROUND(COALESCE(a.fx, -99), 1), 5, 1) AS fx,
  DEC(ROUND(COALESCE(a.fg, -99), 1), 5, 1) AS fg,
  DEC(ROUND(COALESCE(a.t, -99), 1), 5, 1) AS t,
  DEC(ROUND(COALESCE(a.td, -99), 1), 5, 1) AS td,
  DEC(ROUND(COALESCE(a.tx, -99), 1), 5, 1) AS tx,
  DEC(ROUND(COALESCE(a.tn, -99), 1), 5, 1) AS tn,
  -99 AS veghiti,
  DEC(ROUND(COALESCE(a.p, -99), 1), 6, 1) AS p,
  DEC(ROUND(COALESCE(a.pbr, -99), 1), 5, 1) AS pbr,
  DEC(ROUND(COALESCE(a.rh, -99), 1), 5, 1) AS rh,
  DEC(ROUND(COALESCE(a.snd, -99), 1), 5, 1) AS snd,
  (CASE WHEN a.snc is null THEN -99 ELSE a.snc END) AS snc,
  (CASE WHEN a.s is null THEN -99 ELSE a.s END) AS sjolag,
  (CASE WHEN a.n is null THEN -99 ELSE a.n END) AS skyjahula,
  (CASE WHEN a.w is null THEN -99 ELSE a.w END) AS vedur,
  w.vm AS vedurmerki,
  k.skyggni_i_km AS skyggni,
  DEC(ROUND(COALESCE(a.r, -99), 1), 5, 1) AS r,
  (
    CASE WHEN a.r is null THEN -99 else (CASE WHEN HOUR(a.timi) = 9 THEN 900 else 540 END) END
   ) AS rminutes
FROM
  ath.ath a,
  sta.vedurmerki_w_n AS w,
  sta.skyjahula AS n,
  sta.skyggni AS k
WHERE
  a.stod IN (:stations) -- FIXME <isNotNull property="stations"> WHERE a.stod IN (<iterate property="stations" open="" conjunction="," close="">#stations[]#</iterate>) AND </isNotNull>
  AND a.w is null
  AND w.w = '00'
  AND w.n = CAST(a.n AS CHAR(1))
  AND n.n = CAST(a.n AS CHAR(1))
  AND CAST(k.v AS INTEGER) = CAST(a.v AS INTEGER)
  AND a.timi >= :starttime
  AND a.timi <= :endtime
UNION
SELECT
  DISTINCT a.stod AS stod,
  a.timi,
  a.ar,
  a.man,
  a.dagur,
  DEC(
    ROUND(
      COALESCE((CASE a.d WHEN 0 THEN 360 ELSE a.d END), -99),
      1
    ),
    5,
    1
  ) AS d,
  DEC(ROUND(COALESCE(a.f, -99), 1), 5, 1) AS f,
  DEC(ROUND(COALESCE(a.fx, -99), 1), 5, 1) AS fx,
  DEC(ROUND(COALESCE(a.fg, -99), 1), 5, 1) AS fg,
  DEC(ROUND(COALESCE(a.t, -99), 1), 5, 1) AS t,
  -99 AS td,
  DEC(ROUND(COALESCE(a.tx, -99), 1), 5, 1) AS tx,
  DEC(ROUND(COALESCE(a.tn, -99), 1), 5, 1) AS tn,
  DEC(ROUND(COALESCE(a.tn0, -99), 1), 5, 1) AS veghiti,
  -99 AS p,
  -99 AS pbr,
  DEC(ROUND(COALESCE(a.rh, -99), 1), 5, 1) AS rh,
  -99 AS snd,
  -99 AS snc,
  -99 AS sjolag,
  -99 AS skyjahula,
  -99 AS vedur,
  0 AS vedurmerki,
  '-99.0' AS skyggni,
  -99 AS r,
  -99 AS rminutes
FROM
  ath.sj_vg a
WHERE
  a.stod IN (:stations) -- FIXME <isNotNull property="stations"> WHERE a.stod IN (<iterate property="stations" open="" conjunction="," close="">#stations[]#</iterate>) AND </isNotNull>
  AND a.timi >= :starttime
  AND a.timi <= :endtime
UNION
SELECT
  DISTINCT a.stod AS stod,
  a.timi,
  a.ar,
  a.man,
  a.dagur,
  DEC(
    ROUND(
      COALESCE((CASE a.d WHEN 0 THEN 360 ELSE a.d END), -99),
      1
    ),
    5,
    1
  ) AS d,
  DEC(ROUND(COALESCE(a.f, -99), 1), 5, 1) AS f,
  DEC(ROUND(COALESCE(a.fx, -99), 1), 5, 1) AS fx,
  DEC(ROUND(COALESCE(a.fg, -99), 1), 5, 1) AS fg,
  DEC(ROUND(COALESCE(a.t, -99), 1), 5, 1) AS t,
  DEC(ROUND(COALESCE(a.td, -99), 1), 5, 1) AS td,
  DEC(ROUND(COALESCE(a.tx, -99), 1), 5, 1) AS tx,
  DEC(ROUND(COALESCE(a.tn, -99), 1), 5, 1) AS tn,
  -99 AS veghiti,
  DEC(ROUND(COALESCE(a.p, -99), 1), 6, 1) AS p,
  -99 AS pbr,
  DEC(ROUND(COALESCE(a.rh, -99), 1), 5, 1) AS rh,
  -99 AS snd,
  -99 AS snc,
  -99 AS sjolag,
  -99 AS skyjahula,
  -99 AS vedur,
  0 AS vedurmerki,
  '-99.0' AS skyggni,
  -99 AS r,
  -99 AS rminutes
FROM
  ath.sj_klst a
WHERE
  a.stod IN (:stations) -- FIXME <isNotNull property="stations"> WHERE a.stod IN (<iterate property="stations" open="" conjunction="," close="">#stations[]#</iterate>) AND </isNotNull>
  AND a.timi >= :starttime
  AND a.timi <= :endtime
ORDER BY
  1,
  2
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
            bindparam('startime', type_=DateTime),
            bindparam('endtime', type_=DateTime)
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callSecondaryStation(stations):
    """WeatherSecondayStation
    Secondary station information
    Args:
        stations (literal): List of observation stations id. Example 1,2,3,4,5,6,7,8
    
    Returns:
        SecondaryStation: Secondary station information
    """
    sql_str = """
select 
  stod, 
  varastod 
from 
  vefur.stodatridi 
where 
  stod in (:stations)
    -- < iterate open = "" conjunction = "," close = "" > #value[]#</iterate>)
  and varastod is not null order by stod
    """
    sql_txt = text(sql_str)
    try:
        sql_cmd = sql_txt.bindparams(
        )
    except ArgumentError as e:
        print ("Error INTERNAL_SERVER_ERROR " + str(e) +  ". " + str(sql_str))
        #return returnFailure("INTERNAL_SERVER_ERROR", str(e))    
    result = _query(
        sql_txt,
        False
    )
    return result

def callStationForecastResult():
    """WeatherStationForecast
    
    
    Returns:
        StationForecastResult: 
    """
    sql_str = """
select 
  hraspa.stod, 
  hraspa.timi, 
  hraspa.gildistimi, 
  hraspa.likan, 
  hraspa.delta_kl, 
  dec(
    round(hraspa.n, 1), 
    2, 
    1
  ) as n, 
  dec(hraspa.d, 4, 1) as d, 
  dec(
    round(
      COALESCE(hraspa.td,-99), 
      1
    ), 
    3, 
    1
  ) as td, 
  dec(
    round(
      COALESCE(hraspa.rh,-99), 
      1
    ), 
    5, 
    1
  ) as rh, 
  dec(
    round(hraspa.r, 1), 
    5, 
    1
  ) as r, 
  dec(
    round(hraspa.t, 1), 
    3, 
    1
  ) as t, 
  dec(
    round(hraspa.f, 1), 
    3, 
    1
  ) as f, 
  dec(
    round(
      COALESCE(spasia_t.gildi,-99), 
      1
    ), 
    3, 
    1
  ) as t_sia, 
  dec(
    round(
      COALESCE(spasia_f.gildi,-99), 
      1
    ), 
    3, 
    1
  ) as f_sia, 
  dec(
    round(
      COALESCE(spasia_r.gildi,-99), 
      1
    ), 
    3, 
    1
  ) as r_sia 
from 
  spa.hraspa_new hraspa 
  left outer join spa.spasian as spasia_t on hraspa.stod = spasia_t.stod 
  and hraspa.likan = spasia_t.likan 
  and hraspa.timi = spasia_t.timi 
  and hraspa.gildistimi = spasia_t.gildistimi 
  and spasia_t.stiki = 't' 
  left outer join spa.spasia as spasia_f on hraspa.stod = spasia_f.stod 
  and hraspa.likan = spasia_f.likan 
  and hraspa.timi = spasia_f.timi 
  and hraspa.gildistimi = spasia_f.gildistimi 
  and spasia_f.stiki = 'f' 
  left outer join spa.spasia as spasia_r on hraspa.stod = spasia_r.stod 
  and hraspa.likan = spasia_r.likan 
  and hraspa.timi = spasia_r.timi 
  and hraspa.gildistimi = spasia_r.gildistimi 
  and spasia_r.stiki = 'r' 
where 
  hraspa.stod in (:stations) -- < isNotNull property = "stations" > hraspa.stod in (
  -- FIXME   < iterate property = "stations" open = "" conjunction = "," close = "" > #stations[]#</iterate>) and</isNotNull>
  dand hraspa.likan in (:model)  -- hraspa.likan in (
  -- FIXME     < iterate property = "model" open = "" conjunction = "," close = "" > #model[]#</iterate>)
  and hraspa.timi = :time
  order by 
    hraspa.stod,
    hraspa.gildistimi
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result

def callStationForecastModel():
    """WeatherStationForecastModel
    Returns StationForecast model and their properties
    
    Returns:
        StationForecastModel: Returns StationForecast model and their properties
    """
    sql_str = """
SELECT 
  likan, 
  timi, 
  rodun, 
  siun_t, 
  siun_f, 
  siun_r 
FROM 
  vefur.likon_birt 
ORDER BY 
  rodun
    """
    sql_txt = text(sql_str)    
    result = _query(
        sql_txt,
        False
    )
    return result
