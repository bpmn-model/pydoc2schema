#!/usr/bin/env python

"""Tests for `pydoc2schema` package."""

import unittest
import os

from pydoc2schema import pydoc2schema
from pydoc2schema import cli

class TestMainModule(unittest.TestCase):

    def setUp(self):
        self.working_directory = os.path.dirname(os.path.realpath(__file__)) + "/data/warehouse/"

    def test_readdirectory(self):
        """
        Should find all python files in a directory
        """
        files = pydoc2schema.load_directory ( self.working_directory )
        self.assertEqual(len(files), 1)

    def test_extractingbytype_method(self):
        """
        Should extract docstrings from module
        """
        schemas = pydoc2schema.parse_directory ( self.working_directory )
        self.assertEqual(1,1)