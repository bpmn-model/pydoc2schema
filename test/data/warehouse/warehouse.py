"""Warehouse state management

Handles state management for xxx Warehouses. Created initialy for simulation purposes.
Database Redis is used to load / save the state. States include Xyst, Yacht, Zipper

Example:
    To run this you can execute the CLI ::

        $ python main.py

Section breaks are created by resuming unindented text. Section breaks
are also implicitly created anytime a new section starts.

Attributes:
    module_level_variable1 (int): Module level variables may be documented in
        either the ``Attributes`` section of the module docstring, or in an
        inline docstring immediately following the variable.

        Either form is acceptable, but the two should not be mixed. Choose
        one convention to document module level variables and be consistent
        with it.

Todo:
    * Yacht
    * Floor
    * Zipper    

"""

import json

def setXyst(xyst_id, xyst_config):
    """Adds or alters a Xysts robot configuration

    Used to create or change a robot configuration. The configuration is of a schema form.

    Args:
        xyst_id (str): Unique identifier of the Xyst robot
        xyst_config (xyst-schema): Is a schema validated configuration
    """
    xyst_list = getXysts()

def getXyst(xyst_id):
    """List of Xysts robots

    Used to fetch all the Xysts robots that travel along the X axis in a warehouse

    Args:
        xyst_id (str): Unique identifier of the Xyst robot

    Returns:
        xyst-schema: Xyst schema
    """
    xyst_list = getXysts()
    if xyst_id in xyst_list: return xyst_list[xyst_id]

def getXysts():
    """List of Xysts robots

    Used to fetch all the Xysts robots that travel along the X axis in a warehouse

    Returns:
        xysts-schema: An array of Xyst
    """
    return {}